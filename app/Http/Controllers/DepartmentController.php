<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;

class DepartmentController extends Controller
{
     
    public function index()
    {
        return Department::latest()->get();
    }

    public function store(Request $request)
    {   
        $this->validate($request, [
            'name' => 'required',
        ]);

        return Department::create([
           'name' => $request['name'],
        ]);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {   
        $this->validate($request, [
            'name' => 'required',
        ]);

        $user = Department::findOrFail($id);

        $user->update($request->all());
    }

    public function destroy($id)
    {
        $user = Department::findOrFail($id);
        $user->delete();
        return response()->json([
         'message' => 'Department deleted successfully'
        ]);
    }
}
