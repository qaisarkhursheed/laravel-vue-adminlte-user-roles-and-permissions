import VueRouter from 'vue-router';
import Dashboard from './components/admin/DashboardComponent.vue'
import Profile from './components/admin/ProfileComponent.vue'
import User from './components/admin/UserComponent.vue'
import Department from './components/admin/DepartmentComponent.vue'

// let routes = [
//     {
//         path: '/dashboard',
//         component: require('./views/dashboard').default
//     },
//     {
//         path: '/users',
//         component: require('./views/users').default
//     }
// ];


export const routes = [
    {
        path:'/dashboard',
        component:Dashboard
    },
    {
        path:'/profile',
        component:Profile
    },
    { 
        path:'/users',
        component:User
    },
    { 
        path:'/departments',
        component:Department
    },
 
 
];

export default new VueRouter({
    base: '/admin/',
    mode: 'history',
    routes,
    linkActiveClass: 'active'
});
